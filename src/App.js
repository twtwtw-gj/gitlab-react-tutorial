import './App.css';

function App() {
  return (
    <div className="App">
      <div className="text">
        <h1>
          これはGitLab Pagesサイトのサンプルです。
        </h1>
      </div>
      <div className="explain">
        このサイトのソースコードはこちらに置いてあります。
        <br />
        <a href="https://gitlab.com/twtwtw-gj/gitlab-react-tutorial" className="source-code">
          Source Code
        </a>
        <br />
        MITライセンスです。修正・再利用などご自由にお使いください。
      </div>
    </div>
  );
}

export default App;
